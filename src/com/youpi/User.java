package com.youpi;

import java.util.ArrayList;

public class User {
    private String userName;
    private ArrayList userThreePreviousCoworkers = new ArrayList();

    User(String name) {
        userName = name;
    }

    public void addPreviousCoworker(User coworker) {
        userThreePreviousCoworkers.add(coworker);
    }

    public String getUserName() {
        return userName;
    }

    public ArrayList getUserThreePreviousCoworkers() {
        return userThreePreviousCoworkers;
    }
}
