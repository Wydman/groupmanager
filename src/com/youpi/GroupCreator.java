package com.youpi;

import java.util.*;

public class GroupCreator {
    private Scanner sc = new Scanner(System.in);
    private HashMap<User, Integer> sortedList = new HashMap<>();

    GroupCreator(List<User> playerList) {
        createAllGroups(playerList);
    }

    public HashMap<User, Integer> createAllGroups(List<User> playerList) {
        Collections.shuffle(playerList);

        System.out.println("What Group size do you want ?");
        int groupSizeWanted = Integer.parseInt(sc.nextLine());
        List<User> bufferList = new ArrayList<>();
        bufferList.clear();

        for (int i = 0; i <= groupSizeWanted; i++) {
            int iteration = 0;
            for (User user : playerList) {
                if (!bufferList.contains(user) && iteration < playerList.size()/groupSizeWanted) {
                    bufferList.add(user);
                    sortedList.put(user, i);
                    System.out.println(user.getUserName() + " " + i);
                    iteration++;
                }
            }
        }
        return sortedList;
    }
}

