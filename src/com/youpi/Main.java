package com.youpi;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.util.*;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        boolean wantToQuit = false;

        ArrayList<User> userList = new ArrayList<User>(List.of(new User("Charles"), new User("Marion"), new User("Sarah"),
                new User("Cynthia"), new User("Aurélien"), new User("Erwan"),
                new User("Stéphane"), new User("Pierre Alain"), new User("Richard"),
                new User("Maxime"), new User("Joannie"), new User("Baptiste")));

        //   GroupCreator groupCreator = new GroupCreator(userList);
                groupgen groupgen = new groupgen(userList);
                 JFrame jframe = groupgen.DisplayRandomizedGroup();

        while (!wantToQuit) {
            System.out.println("-----Try Again ? y/n-----");
            if (sc.nextLine().equals("n")) {
                jframe.dispose();
                wantToQuit = true;
            } else {
                //groupCreator.createAllGroups(userList);
                jframe.dispose();
                groupgen.GenerateRandomizeGroups(userList);
                jframe = groupgen.DisplayRandomizedGroup();
            }
        }



            /*
            for (Map.Entry<User, Integer> entry : groupCreator.createAllGroups(userList).entrySet()) {
                User user = entry.getKey();
                Integer groupNumber = entry.getValue();
                //   System.out.println(user.getUserName() + " : groupe : " + groupNumber);
            }
            */
    }
}
