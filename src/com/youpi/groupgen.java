package com.youpi;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.util.*;
import java.util.List;

public class groupgen {
    double nbgroupe;
    List<List<User>> finalListOfGroups = new ArrayList<>();

    groupgen(List<User> userList) {
        GenerateRandomizeGroups(userList);
    }

    public void GenerateRandomizeGroups(List<User> userList) {
        finalListOfGroups.clear();
        List<User> bufferList = new ArrayList<>();
        bufferList.addAll(userList);
        Collections.shuffle(bufferList);
        Scanner sc = new Scanner(System.in);
        System.out.println("Combien de groupes ?");
        try {
            nbgroupe = sc.nextDouble();
        } catch (InputMismatchException e) {
            System.out.println("--Cette valeur n'est pas applicable\n--veuillez rentrer une valeur entre 1 et " + bufferList.size());
            return;
        }

        double userPerGroups = bufferList.size() / nbgroupe;

        if (nbgroupe > bufferList.size() || nbgroupe <= 0) {
            System.out.println("--Cette valeur n'est pas applicable\n--veuillez rentrer une valeur entre 1 et " + bufferList.size());
            return;
        } else {
            for (int i = 0; i < nbgroupe; i++) {
                finalListOfGroups.add(RandomizeForWholeNumberOfUserPerGroups(bufferList, userPerGroups));
            }
        }
        if (!bufferList.isEmpty()) {
            int i = 0;
            while (!bufferList.isEmpty()) {
                finalListOfGroups.get(i).add(bufferList.get(0));
                bufferList.remove(0);
                i++;
            }
        }
    }

    private List<User> RandomizeForWholeNumberOfUserPerGroups(List<User> bufferList, double userPerGroups) {
        List<User> groupList = new ArrayList<>();
        for (int i2 = 0; i2 < (int) userPerGroups; i2++) {
            int userIndex = new Random().nextInt(bufferList.size());
            groupList.add(bufferList.get(userIndex));
            bufferList.remove(userIndex);
        }
        return groupList;
    }

    public JFrame DisplayRandomizedGroup() {

        JPanel panel = new JPanel(null);

        JFrame frame = new JFrame();
        frame.setPreferredSize(new Dimension(800, 600));
        frame.setVisible(true);
        frame.setSize(800, 600);
        frame.add(panel);

        List<Point> pointList = new ArrayList<>(List.of(new Point(50, 100), new Point(150, 100), new Point(250, 100),
                new Point(350, 100), new Point(450, 100), new Point(550, 100),
                new Point(50, 300), new Point(150, 300), new Point(250, 300),
                new Point(350, 300), new Point(450, 300), new Point(550, 300),
                new Point(50, 0)));


        int prout = 0;

        for (List<User> list : finalListOfGroups) {
            for (User user : list) {
                System.out.println(user.getUserName() + " -- Groupe  : " + (finalListOfGroups.indexOf(list) + 1));
                JLabel helloLabel = new JLabel(user.getUserName()+System.lineSeparator()+" "+(finalListOfGroups.indexOf(list) + 1));
                helloLabel.setBounds(pointList.get(prout).x, pointList.get(prout).y, 200, 200); // x, y, width, height
                panel.add(helloLabel);
                System.out.println(user.getUserName() + "  " +pointList.get(prout).x);
                prout++;
            }
        }
        frame.repaint();
        return frame;
    }
}
